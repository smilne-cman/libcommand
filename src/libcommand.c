#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libcollection/list.h>
#include <libcollection/map.h>
#include <libcollection/types.h>
#include <libcollection/compare.h>
#include <libcollection/iterator.h>
#include "libcommand.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void *default_resolver(List *words);
static List *from_string(const char *text, const char *separator);

/* Global Variables ***********************************************************/
static int exit_loop = FALSE;

/* Functions ******************************************************************/
CommandHandler *command_new(const char *trigger, ResolverFn resolver, HandlerFn handler) {
	CommandHandler *command = (CommandHandler *)malloc(sizeof(CommandHandler));

	command->trigger = trigger;
	command->resolver = resolver == NULL ? default_resolver : resolver;
	command->handler = handler;
	command->subcommands = map_new(map_type_new(string_compare, void_compare));

	return command;
}

void command_add(CommandHandler *command, CommandHandler *subcommand) {
	 map_put(command->subcommands, subcommand->trigger, subcommand);
}

void command_destroy(CommandHandler *handler) {
	Iterator *iterator = map_iterator(handler->subcommands);
	while(has_next(iterator)) {
		MapEntry *entry = next_map(iterator);
		CommandHandler *subcommand = (CommandHandler *)entry->value;

		command_destroy(subcommand);
	}

	iterator_destroy(iterator);
	map_destroy(handler->subcommands);
	free(handler);
}

void command_invoke(CommandHandler *root, List *words) {
	if (list_size(words) < 1) {
		void *data = root->resolver(words);
		if (data != NULL) root->handler(data);
		return;
	}

	const char *commandString = list_gets(words, 0);

	if (map_contains(root->subcommands, commandString)) {
		CommandHandler *subcommand = (CommandHandler *)map_get(root->subcommands, commandString);
		list_remove(words, 0);
		command_invoke(subcommand, words);

		return;
	}

	void *data = root->resolver(words);
	if (data != NULL) root->handler(data);
}

void command_prompt(CommandHandler *root, const char *text, int length) {
  while(exit_loop == FALSE) {
    List *args = from_string(prompt(text, length), " ");
    list_prints(args);

    command_invoke(root, args);
    free(args);
  }

  exit_loop = FALSE;
}

List *from_args(int count, char *args[]) {
	List *words = list_new(ListString);

	for (int i = 1; i < count; i++) {
		list_adds(words, args[i]);
	}

	return words;
}

void command_abort() {
  exit_loop = TRUE;
}

static void *default_resolver(List *words) {
	return words;
}

static List *from_string(const char *text, const char *separator) {
  List *words = list_new(ListString);
  char *word = split(text, separator);

  while(word != NULL) {
    char *buffer = (char *)malloc(sizeof(char) * 256);
    strcpy(buffer, word);

    list_adds(words, buffer);

    word = strtok(NULL, separator);
  }

  return words;
}
