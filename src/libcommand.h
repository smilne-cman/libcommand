#ifndef __libcommand__
#define __libcommand__

/* Includes *******************************************************************/
#include <libcollection/list.h>
#include <libcollection/map.h>

/* Types **********************************************************************/
typedef void *(*ResolverFn)(List *arguments);
typedef void (*HandlerFn)(void *data);

typedef struct CommandHandler {
	const char *trigger;			// String which triggers this command
	Map *subcommands;					// Collection of subcommands which could be triggered
	ResolverFn resolver;			// Function invoked to generate a data struct from an argument list
	HandlerFn handler;				// Function invoked when command is triggered
} CommandHandler;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
CommandHandler *command_new(const char *trigger, ResolverFn resolver, HandlerFn handler);
void command_add(CommandHandler *command, CommandHandler *subcommand);
void command_destroy(CommandHandler *handler);

void command_invoke(CommandHandler *root, List *words);
void command_prompt(CommandHandler *root, const char *text, int length);
List *from_args(int count, char *args[]);
void command_abort();

#endif
