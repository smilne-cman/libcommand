#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include "libcommand.h"

/* Types **********************************************************************/
typedef struct TestData {
	int value;
} TestData;

/* Prototypes *****************************************************************/
void test_invoke();
void test_resolver();
void test_from_args();

TestData *resolve_test(List *words);
void on_resolved_test(TestData *data);
void on_unknown(List *arguments);
void on_test(List *arguments);
void on_children(List *arguments);
void on_child1(List *arguments);
void on_child2(List *arguments);

/* Global Variables ***********************************************************/
static const char *lastCalled = NULL;
static List *lastArgs = NULL;
static int lastCount = 0;

int main(int argc, char *argv[]) {
  test_init("libcommand", argc, argv);

  test("Invoke", test_invoke);
	test("Resolver", test_resolver);
	test("From Args", test_from_args);

  return test_complete();
}

void test_invoke() {
  CommandHandler *interpreter = command_new("", NULL, on_unknown);
	command_add(interpreter, command_new("test", NULL, on_test));

	CommandHandler *children = command_new("children", NULL, on_children);
	command_add(children, command_new("child1", NULL, on_child1));
	command_add(children, command_new("child2", NULL, on_child2));

	command_add(interpreter, children);

	List *words = list_new(ListString);
	list_adds(words, "children");
	list_adds(words, "child2");

	command_invoke(interpreter, words);

	test_assert_string(lastCalled, "child2", "Should call correct handler");
	test_assert_int(list_size(lastArgs), 0, "Should not provide any arguments");

	list_destroy(words);
	command_destroy(interpreter);
}

void test_resolver() {
	CommandHandler *root = command_new("", NULL, on_unknown);
	command_add(root, command_new("test", resolve_test, on_resolved_test));

	List *words = list_new(ListString);
	list_adds(words, "test");
	list_adds(words, "abc");
	list_adds(words, "123");

	command_invoke(root, words);

	test_assert_string(lastCalled, "resolved_test", "Should invoke correct handler");
	test_assert_int(lastCount, 2, "Should pass the result of the resolver into the handler");

	list_destroy(words);
	command_destroy(root);
}

void test_from_args() {
	const char *args[3];
	args[0] = "blah";
	args[1] = "hello";
	args[2] = "world";

	List *words = from_args(3, args);

	test_assert_int(list_size(words), 2, "Should add all but the first argument to the list");
}

TestData *resolve_test(List *words) {
	TestData *data = (TestData *)malloc(sizeof(TestData));

	data->value = list_size(words);

	return data;
}

void on_resolved_test(TestData *data) {
	lastCalled = "resolved_test";
	lastCount = data->value;
}

void on_unknown(List *arguments) {
	lastCalled = "unknown";
	lastArgs = arguments;
}

void on_test(List *arguments) {
	lastCalled = "test";
	lastArgs = arguments;
}

void on_children(List *arguments) {
	lastCalled = "children";
	lastArgs = arguments;
}

void on_child1(List *arguments) {
	lastCalled = "child1";
	lastArgs = arguments;
}

void on_child2(List *arguments) {
	lastCalled = "child2";
	lastArgs = arguments;
}
